﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FindingPrimes
{
    class Program
    {
        static void Main()
        {
            int[][] data = new int[Int32.Parse(Console.ReadLine())][];
            for (var i = 0; i < data.Length; i++)
            {
                data[i] = new int[2];
                data[i] = Console.ReadLine().Split(' ').Select(x => Int32.Parse(x)).ToArray();
            }
            foreach (var item in data)
            {
                Console.WriteLine(FindPrimes(item[0], item[1]));
            }
            Console.ReadKey();
            }

        private static int FindPrimes(int start, int end)
        {
            int counter = 0;
            var listOfPrimes = new List<int>
            {
                2
            };
            for (int i = 2; i <= (end + 1) / 2; i++)
            {
                listOfPrimes.Add(2 * i - 1);
            }

            for (int j = 0; j < listOfPrimes.Count; ++j)
            {
                int prime = listOfPrimes[j];

                for (int multiple = 2; multiple * prime <= end; ++multiple)
                {
                    if (prime == 2) break;

                    if ((multiple * prime) % 2 == 0)
                        continue;

                    if (!listOfPrimes.Contains(multiple * prime))
                        continue;

                    listOfPrimes.Remove(multiple * prime);
                }
            }
            
            foreach(var prime in listOfPrimes)
            {
                if (prime > start && prime < end)
                {
                    counter++;
                }
            }
            return counter;
        }
    }
}
